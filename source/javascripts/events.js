(function() {
  function filterDuplicatesArray(a) {
    var seen = {};
    var out = [];
    var len = a.length;
    var j = 0;
    for (var i = 0; i < len; i++) {
      var item = a[i];
      if (seen[item] !== 1) {
        seen[item] = 1;
        out[j++] = item;
      }
    }
    return out;
  }

  var getItems = function(total, page) {
    var items = [];
    if (page > 1) items.push({ title: '<< First', where: 1 });
    if (page > 1) {
      items.push({ title: 'Prev', where: page - 1 });
    } else {
      items.push({ title: 'Prev', where: page - 1, disabled: true });
    }
    if (page > 6) items.push({ title: '...', separator: true });

    var start = Math.max(page - 4, 1);
    var end = Math.min(page + 4, total);

    for (var i = start; i <= end; i++) {
      var isActive = i === page;
      items.push({ title: i, active: isActive, where: i });
    }

    if (total - page > 4) items.push({ title: '...', separator: true });

    if (page === total) {
      items.push({ title: 'Next', where: page + 1, disabled: true });
    } else if (total - page >= 1) {
      items.push({ title: 'Next', where: page + 1 });
    }

    if (total - page >= 1) items.push({ title: 'Last ', where: total });

    return items;
  };

  var setDropdown = function(event) {
    var $selected = $(event.currentTarget);

    var newTitle = $selected.find('.value').text();
    var $title = $selected.parents('.dropdown').find('.dropdown-title');

    $title.text(newTitle);
  };

  var DESIRED_ELEMENTS_PER_PAGE = 15;

  this.EventsHandler = (function() {
    function EventsHandler() {
      this.eventListDOM = $('.event-list li');
      this.render();
    }

    EventsHandler.prototype.bindEvents = function() {
      var $eventTypeDropdown = $('#event-types li');
      var $eventLocationDropdown = $('#event-location li');

      // Set selected dropdown value
      $eventTypeDropdown.on('click', setDropdown);
      $eventLocationDropdown.on('click', setDropdown);

      // Render the filtered content
      $eventTypeDropdown.on('click', this.render.bind(this));
      $eventLocationDropdown.on('click', this.render.bind(this));
    };

    EventsHandler.prototype.renderEventList = function(options) {
      var $eventsList = $('.event-list');
      var eventsToPopulate = options.renderFilteredData ? options.filteredData : this.eventListDOM;
      if (this.currentPage < 1) {
        this.currentPage = 1;
        return;
      } else if (this.currentPage > this.getTotalPages(eventsToPopulate.length)) {
        this.currentPage = this.getTotalPages(eventsToPopulate.length);
        return;
      }

      $eventsList.empty();

      var eventListArray = [];
      $('.pagination li span').off('click');
      if (eventsToPopulate.length > DESIRED_ELEMENTS_PER_PAGE) {
        document.querySelector('.pagination').innerHTML =
        getItems(this.getTotalPages(eventsToPopulate.length), this.currentPage)
          .map(function(e) {
            var activeElement;
            if (e.active) {
              activeElement = '<li seperator="' + e.seperator + '" class="active" disabled="' + e.disabled + '">'
                              + '<span>' + e.title + '</span>'
                              + '</li>';
            } else {
              activeElement = '<li seperator="' + e.seperator + '" disabled="' + e.disabled + '">'
                              + '<span>' + e.title + '</span>'
                              + '</li>';
            }
            return activeElement;
          }).join('');
        $('.pagination li:not(.active) span').on('click', this.render.bind(this));
      } else {
        document.querySelector('.pagination').innerHTML = '';
      }

      // Create the event list based on the data that we have filtered and page
      for (var i = (this.currentPage - 1) * DESIRED_ELEMENTS_PER_PAGE;
        i < (this.currentPage * DESIRED_ELEMENTS_PER_PAGE); i += 1) {
        var element = eventsToPopulate[i];
        if (element) {
          eventListArray.push(eventsToPopulate[i]);
        } else {
          break;
        }
      }

      $eventsList.append(eventListArray).hide().fadeIn();
      $('.event-list .event-headers .event-topic').off('click').on('click', this.displayEventDescription);
      // Populate dropdowns based on the current data shown
      if (options.renderFilteredData) {
        this.populateDropdowns(options.filteredData, false);
      } else {
        this.populateDropdowns(this.eventListDOM, true);
      }
    };

    EventsHandler.prototype.displayEventDescription  = function(e) {
      var currentArrow = $(e.currentTarget);
      var $parentElement = $(currentArrow).parent().parent();
      if (currentArrow.hasClass('is-open')) {
        currentArrow.removeClass('is-open');
        $($parentElement).find('.event-description').hide();
      } else {
        currentArrow.addClass('is-open');
        $($parentElement).find('.event-description').fadeIn();
      }
    };

    EventsHandler.prototype.filterData = function() {
      var filters = this.getFilterValues();
      filters.type = filters.type.toLowerCase();
      filters.location = filters.location.toLowerCase();
      var filteredData = this.eventListDOM.filter(function() {
        if (filters.type === 'event type' && filters.location === 'event location') {
          return true;
        }

        var eventType = $(this).find('.event-type').first().text().trim().toLowerCase();
        var eventLocation = $(this).data('region').toLowerCase();

        var filterByType = filters.type !== 'any event type';
        var filterByLocation = filters.location !== 'any event location';

        var include = true;

        if (filterByType) {
          include = (eventType === filters.type) ? include : false;
        }

        if (filterByLocation) {
          include = (eventLocation === filters.location) ? include : false;
        }

        return include;
      });
      return filteredData;
    };

    EventsHandler.prototype.render = function(e) {
      function renderData(shouldWeFilter, pageNumber) {
        var filteredData;
        if (shouldWeFilter && !pageNumber) {
          filteredData = this.filterData.call(this);
          this.currentPage = 1;
          this.renderEventList({renderFilteredData: true, filteredData: filteredData});
        } else if (shouldWeFilter && pageNumber) {
          filteredData = this.filterData.call(this);
          var newPageNumber;
          if (pageNumber.text().toLowerCase() === 'next') {
            newPageNumber = this.currentPage += 1;
          } else if (pageNumber.text().toLowerCase() === 'prev') {
            newPageNumber = this.currentPage -= 1;
          } else if (pageNumber.text().toLowerCase().indexOf('first') !== -1) {
            newPageNumber = this.currentPage = 1;
          } else if (pageNumber.text().toLowerCase().indexOf('last') !== -1) {
            newPageNumber = this.getTotalPages(filteredData.length);
          } else {
            newPageNumber = parseInt(pageNumber.text(), 10);
          }
          this.currentPage = newPageNumber;
          this.renderEventList({renderFilteredData: true, filteredData: filteredData});
        } else {
          this.currentPage = 1;
          this.renderEventList({renderFilteredData: false});
        }
      }
      if (typeof e !== 'undefined') {
        var $callingElement = $(e.currentTarget);
        var classCallingElement = $callingElement.parent()[0].className;
        if (classCallingElement.indexOf('dropdown-menu') !== -1) {
          renderData.call(this, true);
        } else {
          renderData.call(this, true, $callingElement);
        }
      } else {
        renderData.call(this, false);
      }
    };

    EventsHandler.prototype.updateDropdownData = function(currentlyDisplayedData) {
      var dropdownData = {
        types: ['Any Event Type'],
        locations: ['Any Event Location']
      };
      currentlyDisplayedData.map(function() {
        var eventLocation = $(this).data('region');
        dropdownData.locations.push(eventLocation);
      });
      this.eventListDOM.map(function() {
        var eventType = $(this).find('.event-type').first().text().trim();
        dropdownData.types.push(eventType);
      });
      dropdownData.types = filterDuplicatesArray(dropdownData.types);
      dropdownData.locations = filterDuplicatesArray(dropdownData.locations);
      return dropdownData;
    };

    EventsHandler.prototype.populateDropdowns = function(currentlyDisplayedData, firstExecution) {
      var eventTypesDisplayArray = [];
      var eventLocationDisplayArray = [];
      var filterValues = this.getFilterValues();
      var currentDropdownData = this.updateDropdownData.call(this, currentlyDisplayedData);
      // Populate the dropdowns
      var $eventTypeDropdown = $('#event-types ul');
      var $eventLocationDropdown = $('#event-location ul');
      // The template for the dropdown list
      $eventTypeDropdown.empty();
      currentDropdownData.types.forEach(function(type) {
        $eventTypeDropdown.append('<li><span class="value">' + type + '</span></li>');
      });
      $eventLocationDropdown.empty();
      currentDropdownData.locations.forEach(function(location) {
        $eventLocationDropdown.append('<li><span class="value">' + location + '</span></li>');
      });
      $eventTypeDropdown.append(eventTypesDisplayArray);
      $eventLocationDropdown.append(eventLocationDisplayArray);
      // Set title to the first element of the array
      if (firstExecution) {
        $('#event-types .dropdown-title').text('Any Event Type');
        $('#event-location .dropdown-title').text('Any Event Location');
      } else {
        $('#event-types .dropdown-title').text(filterValues.type);
        $('#event-location .dropdown-title').text(filterValues.location);
      }

      this.bindEvents();
    };

    EventsHandler.prototype.getFilterValues = function() {
      return {
        type: $('#event-types').find('.dropdown-title').text() || '',
        location: $('#event-location').find('.dropdown-title').text() || ''
      };
    };

    EventsHandler.prototype.getTotalPages = function(dataLength) {
      return Math.ceil(dataLength / DESIRED_ELEMENTS_PER_PAGE);
    };

    return EventsHandler;
  })();

  // eslint-disable-next-line no-undef, no-new
  new EventsHandler();
})();
