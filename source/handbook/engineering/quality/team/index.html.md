---
layout: markdown_page
title: "Quality Team"
---

## Onboarding

Copy the following Markdown into a new issue in [QA Team Tasks](https://gitlab.com/gitlab-org/quality/team-tasks/issues/new)
and complete the issue.

```markdown
- [ ] Complete your [Onboarding task](https://gitlab.com/gitlab-com/people-ops/employment/issues)
- [ ] Finish up your first Merge Request given to you by the Quality Engineer
- [ ] Checkout the required source trees for Quality
  * [ ] [GitLab-CE](https://gitlab.com/gitlab-org/gitlab-ce)
  * [ ] [GitLab-QA](https://gitlab.com/gitlab-org/gitlab-qa)
- [ ] Read through the [GitLab QA Documentation](https://about.gitlab.com/handbook/engineering/quality/team) index
- [ ] Ensure you have access to the GitLab 1Password Team Vault
- [ ] Ensure you have access to [GitLab Dev](https://dev.gitlab.com) environment
- [ ] Ensure you have access to [GitLab Staging](https://staging.gitlab.com) environment
```

## Links

- Gitlab QA
  - [Testing Guide / E2E Tests](https://docs.gitlab.com/ee/development/testing_guide/end_to_end_tests.html)
  - [GitLab QA Orchestrator Documentation](https://gitlab.com/gitlab-org/gitlab-qa/blob/master/README.md)
  - [GitLab QA Testing Documentation](https://gitlab.com/gitlab-org/gitlab-qa/blob/master/docs/README.md)
- Tests
  - [Testing Standards](https://docs.gitlab.com/ee/development/testing_guide/index.html)
- CI infrastructure for CE and EE
  - [Testing from CI](https://docs.gitlab.com/ee/development/testing_guide/ci.html)
- Tests statistics
  - [Redash Test Suite Statistics](https://redash.gitlab.com/dashboard/test-suite-statistics)
- Insights dashboard
  - [Quality Dashboard](http://quality-dashboard.gitlap.com/)
  - [Quality Dashboard Documentation](https://gitlab.com/gitlab-org/gitlab-insights/blob/master/README.md)
- QA Runners
  - [QA Runner Ownership Issue](https://gitlab.com/gitlab-org/gitlab-qa/issues/261)
- Access to projects / Permissions in GCP for review apps GCP
  - Ensure you have access to the following projects
    * [gitlab-org](https://gitlab.com/gitlab-org)
    * [gitlab-com](https://gitlab.com/gitlab-com)
    * [dev.gitlab.com](https://dev.gitlab.com)
    * [staging.gitlab.com](https://staging.gitlab.com)
