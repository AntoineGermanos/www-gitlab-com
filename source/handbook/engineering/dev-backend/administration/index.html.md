---
layout: markdown_page
title: "Administration Team"
---

## On this page
{:.no_toc}

- TOC
{:toc}

### Administration Team
{: #administration}

The responsibilities of this team are described by the [Administration product
category](/handbook/product/categories/#dev).
