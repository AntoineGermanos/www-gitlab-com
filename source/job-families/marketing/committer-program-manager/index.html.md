---
layout: job_page
title: "Committer program manager"
---

As the Committer Program Manager, you will be responsible for enabling members from the wider GitLab community to contribute code and documentation to GitLab. The goal is to increase the number of unique people that contribute to GitLab every month. Committers are all people who contribute code and documentation to GitLab, not just the people that can merge their contributions (have the commit bit).

## Responsibilities

* Grow the number of unique contributors to GitLab per month.
* Run a complete funnel around contributors, similar to a marketing and sales funnel, including stage criteria, conversion rates, and time per stage per cohort.
* Ensure there are metrics around committers, merges, helping with reviews, and geographic and organizational diversity.
* Ensure all the steps in the committer lifecycle are defined and have actions, for example if you first contribution gets merged we offer a call with a full-time developer on that functionality.
* Make it easy to contribute, for example make it easier to find [issues marked 'accepting merge requests'](https://gitlab.com/gitlab-org/gitlab-ce/blob/master/CONTRIBUTING.md#label-for-community-contributors-accepting-merge-requests)
* Stimulate and organize pairing, mentorships, hack-fests, and internship programs.
* Recognize people in the community, for example with committer badges, swag, spotlights, and awards.

## Requirements

* You have 5-7 years of experience running developer relations or community advocacy programs, preferably open source in nature.
* Analytical and data driven in your approach to building and nurturing communities.
* You have experience facilitating sensitive and complex community situations with humility, empathy, judgment, tact, and humor.
* Excellent spoken and written English.
* Familiarity with developer tools, Git, Continuous Integration, Containers, and Kubernetes.
* Experience in communicating with writers on a range of technical topics is a plus.
* You share our [values](/handbook/values/), and work in accordance with those values.
